# Generated by Django 3.1.4 on 2021-05-13 12:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('memeapp', '0035_auto_20210512_1517'),
    ]

    operations = [
        migrations.CreateModel(
            name='LogInline',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('user_id', models.IntegerField()),
                ('template_id', models.IntegerField()),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('response_time', models.IntegerField()),
            ],
        ),
    ]
