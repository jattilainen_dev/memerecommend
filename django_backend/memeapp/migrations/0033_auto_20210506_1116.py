# Generated by Django 3.1.7 on 2021-05-06 11:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('memeapp', '0032_auto_20210503_1311'),
    ]

    operations = [
        migrations.AlterField(
            model_name='meme',
            name='text',
            field=models.CharField(default='', max_length=5000),
        ),
    ]
