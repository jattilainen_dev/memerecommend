# Generated by Django 3.1.4 on 2021-01-09 12:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('memeapp', '0017_auto_20210104_2034'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='rejected',
            field=models.IntegerField(default=0),
        ),
    ]
